from typing import Optional

from fastapi import FastAPI
from pydantic import BaseModel

import requests
import json


app = FastAPI()


class Deck(BaseModel):
    deck_id: str


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/creer-un-deck/")
def get_deck():
    response = requests.get("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1")
    response = response.json()
    return {"deck_id": response["deck_id"]}


@app.post("/cartes/{nombre_cartes}")
async def draw_card(deck: Deck, nombre_cartes: int):
    response = requests.get("https://deckofcardsapi.com/api/deck/"+deck.deck_id+"/draw/?count={}".format(nombre_cartes))
    response = response.json()
    return {"deck_id": deck.deck_id, "cards": response["cards"]}
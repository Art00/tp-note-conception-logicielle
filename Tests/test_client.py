import unittest
from Client.main import count_colors

class testClient(unittest.TestCase):

    def test_count_colors(self):
        cards = {'cards': 
        [{'code': 'QH', 'value': 'QUEEN', 'suit': 'HEARTS'}
        , {'code': '8D', 'value': '8', 'suit': 'DIAMONDS'}
        , {'code': '7S', 'value': '7', 'suit': 'SPADES'}
        , {'code': '0S', 'value': '10', 'suit': 'SPADES'}
        , {'code': '8C', 'value': '8', 'suit': 'CLUBS'}
        , {'code': 'JC', 'value': 'JACK', 'suit': 'CLUBS'}
        , {'code': '2C', 'value': '2', 'suit': 'CLUBS'}
        , {'code': '3S', 'value': '3', 'suit': 'SPADES'}
        , {'code': '0C', 'value': '10', 'suit': 'CLUBS'}
        , {'code': '4D', 'value': '4', 'suit': 'DIAMONDS'}]
        }
        test_colors = count_colors(cards["cards"])
        true_colors = {'H': 1, 'S': 3, 'D': 2, 'C': 4}

        self.assertEqual(true_colors, test_colors)

if __name__ == "__main__":
    unittest.main()
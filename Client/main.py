import requests
import json
from pydantic import BaseModel
from typing import Optional


class Deck(BaseModel):
    deck_id: str


def init_deck():
    """ Permet d'initialiser un deck de 52 cartes et de retourner son id sous format {"deck_id": str}
    """
    response = requests.get("http://localhost:8000/creer-un-deck/")
    deck = response.json()
    return deck


def draw_cards(nombre_cartes: int, deck: Optional[Deck] = None):
    """ Permet de piocher un nombre de cartes dans un deck et de retouner les cartes piochées avec l'id du deck
        Cas où plus assez de cartes dans le deck: 
            - pioche les cartes qu'il peut dans ce deck
            - prend un nouveau deck mélangé
            - remplace l'id de l'ancien deck
            - pioche les cartes manquantes dans le nouveau deck
            - ajoute aux anciennes cartes les cartes piochées dans le nouveau deck
        Cas où pas de deck: 
            - argument de deck égal à None par défaut
            - récupère l'id d'un nouveau deck mélangé 
            - pioche les cartes dans ce deck
    """
    if deck == None:
        deck = init_deck()
    response = requests.post("http://localhost:8000/cartes/{}".format(nombre_cartes), json=deck)
    cards = response.json()
    if len(cards["cards"]) < nombre_cartes:
        nombre_cartes_manquantes = nombre_cartes-len(cards["cards"])
        new_cards = draw_cards(nombre_cartes_manquantes)
        cards["deck_id"] = new_cards["deck_id"]
        cards["cards"] += new_cards["cards"]
    return cards

def count_colors(cards: list):
    """ Permet de compter le nombre de cartes de chaque couleur piochées et de retouner le résultat sous le format {"H":int,"S":int,"D":int,"C":int}
    """
    colors = {"H":int,"S":int,"D":int,"C":int}
    colors["H"] = 0
    colors["S"] = 0
    colors["D"] = 0
    colors["C"] = 0
    for card in cards:
        if card["suit"] == "SPADES":
            colors["S"] += 1
        elif card["suit"] == "HEARTS":
            colors["H"] += 1
        elif card["suit"] == "CLUBS":
            colors["C"] += 1
        elif card["suit"] == "DIAMONDS":
            colors["D"] += 1
    return colors

if __name__ == "__main__":
    deck = init_deck()
    print(deck)
    cards = draw_cards(10, deck)
    print(cards)
    colors = count_colors(cards["cards"])
    print(colors)
# Wanna play some Batailles ?

***

Le TP comporte trois parties, la mise en place de l'API, puis celle du client de l'API, et enfin un petit test de la fonction permettant de compter le nombre de cartes de chaque couleurs dans une liste.

## API
Dans un premier terminal, lancer l'API à l'aide de l'enchaînement de commandes suivant :
```
$ cd API
$ pip install -r requirements.txt
$ uvicorn main:app --reload
```
On peut accéder à celle-ci via l'adresse : http://127.0.0.1:8000/docs#/

## Client
Dans un second terminal, vous pouvez lancer le client qui effectue les tâches attendues, pour cela utilisez les commandes suivantes :
```
$ cd Client
$ pip install -r requirements.txt
$ python main.py ou $ python3 main.py
```

## Test unitaire
Enfin, vous pouvez lancer le test unitaire en utilisant la commande suivante à partir de la racine :
```
$ python -m unittest Tests/test_client.py ou $ python3 -m unittest Tests/test_client.py
```
